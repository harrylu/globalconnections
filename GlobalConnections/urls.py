"""GlobalConnections URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static

from website import views
from website.views import AboutPageView, HomePageView

urlpatterns = [
    url(r'^$', HomePageView.as_view(template_name = "index.html", action = "index")),
    url(r'^aboutus/$', AboutPageView.as_view(template_name = "aboutus.html", action = "aboutus")),
    url(r'^qanda/$', AboutPageView.as_view(template_name = "qanda.html", action = "qanda", page_type = 'Q')),
    url(r'^culture/$', AboutPageView.as_view(template_name = "culture.html", action = "culture", page_type = 'C')),
    url(r'^skills/$', AboutPageView.as_view(template_name = "skills.html", action = "skills", page_type = 'S')),
    url(r'^testimonials/$', AboutPageView.as_view(template_name = "testimonials.html", action = "testimonials", page_type = 'T')),
    url(r'^contactus/$', views.contactus, name = "contactus"),
    url(r'^feedback/$', views.feedback, name = 'feedback'),
    url(r'^register/$', views.register, name = 'register'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
