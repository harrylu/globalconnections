from datetime import datetime
import hashlib
import json
import os
import random
import urllib
from uuid import uuid4
import tempfile

from django.conf import settings
from django.contrib.syndication.views import Feed
from django.core.exceptions import PermissionDenied
from django.core.mail import send_mail, BadHeaderError
from django.core.management.commands.dumpdata import Command
from django.http import HttpResponseNotFound, HttpResponse, Http404, \
    JsonResponse
from django.shortcuts import render_to_response, get_object_or_404, render, \
    redirect
from django.template.loader import render_to_string
from django.utils.feedgenerator import Atom1Feed
from django.views.decorators.cache import cache_control
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import ensure_csrf_cookie
import time

from .models import HRPage, MainPage, AboutPage, Blog, Faq, HRMainPage, \
    RiskTest
from .questionnaire import calculate_risk, get_portfolios

import logging

log = logging.getLogger(__name__)

DAY = 24 * 3600
VERSION_TAG_COOKIE_EXPIRY = 30 * DAY


def index(request):
    """/ page"""
    explicit_version = False
    set_cookie = False

    if 'version' in request.GET:
        # User asked for an explicit version
        # Ignore any cookies and just serve that one
        tag = request.GET['version']
        explicit_version = True
        log.info('Main page version requested: %s', tag)
        #::type: MainPage
        page = get_object_or_404(MainPage.objects.accessible(), version_tag=tag)
    else:
        # Pick one for the user and make it stick
        page = None

        if 'version_tag' in request.COOKIES:
            # Use has a version tag cookie, serve the same version if still
            # available
            tag = request.COOKIES['version_tag']
            try:
                page = MainPage.objects.enabled().get(version_tag=tag)
            except MainPage.DoesNotExist:
                # User has a cookie for a page that's no longer available
                page = None

        if not page:
            # We need to pick a version
            versions = list(MainPage.objects.enabled())
            if not versions:
                return HttpResponseNotFound("No enabled MainPage versions")
            #::type: MainPage
            page = random.choice(versions)
            log.info('Picked version: %s', page.version_tag)
            set_cookie = True

    context = dict(
        page=page,
        explicit_version=explicit_version,
        is_home=True
    )
    response = render(request, 'index.html', context)
    if set_cookie:
        response.set_cookie('version_tag', page.version_tag,
            max_age=VERSION_TAG_COOKIE_EXPIRY)
    return response


def faq(request):
    faqs = Faq.objects.filter(active = True)
    faqs = sorted(faqs, key = lambda x: x.category.order)
    context = dict(faqs = faqs)
    return render_to_response('faq.html', context)


def hr(request, slug=None):
    about_menu_pages = AboutPage.objects.filter(in_menu=True)
    positions = HRPage.objects.filter(in_menu=True, accessible=True)
    if slug:
        try:
            hr_page = HRPage.objects.get(slug=slug, accessible=True)
        except HRPage.DoesNotExist:
            return redirect('/hr/?not_found={}'.format(slug))

        context = dict(
            menu_pages=about_menu_pages,
            positions=positions,
            page=hr_page,
            title=hr_page.title,
            description=hr_page.heading,
            is_about=True,
            is_hr=True,
            is_position_page=True
        )
        return render_to_response('hr.html', context)
    else:
        page = get_object_or_404(HRMainPage)
        context = dict(
            page=page,
            menu_pages=about_menu_pages,
            positions=positions,
            is_about=True,
            is_hr=True
        )
        return render_to_response('hr_list.html', context)


def about(request, slug=None):
    about_menu_pages = AboutPage.objects.filter(in_menu=True, accessible=True)
    if slug:
        about_page = get_object_or_404(AboutPage, slug=slug, accessible=True)
    else:
        try:
            about_page = about_menu_pages[0]
        except IndexError:
            raise Http404()

    investor = about_page.content.investor
    investor = [investor[i: i+2] for i in range(0, len(investor), 2)]
    context = dict(
        page=about_page,
        menu_pages=about_menu_pages,
        is_about=True,
        investor = [investor[i: i+2] for i in range(0, len(investor), 2)],
    )
    return render_to_response('about.html', context)


def blog_list(request):
    posts = Blog.objects.filter(published=True)

    # FIXME Allow customize paginator number: move it to setting
    paginator = Paginator(posts, 10)
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    context = dict(
        posts=posts,
        menu_pages = AboutPage.objects.filter(in_menu=True),
        description="", # FIXME
        is_list_page=True,
        is_blog=True
    )
    return render_to_response('blog_list.html', context)


def blog_list_json(request):
    posts = Blog.objects.filter(published=True)
    json_posts = []
    for post in posts:

        thumbnail_url = ""
        if post.thumbnail:
            thumbnail_url = request.build_absolute_uri(post.thumbnail.url)
        content = "".join([c.render() for c in post.content.content])
        item = dict(
            title=post.title,
            slug=post.slug,
            author=post.author.name,
            date=post.date,
            language=post.language,
            description=post.description,
            thumbnail=thumbnail_url,
            content=content
        )
        json_posts.append(item)

    response = JsonResponse(json_posts, safe=False)
    response['Access-Control-Allow-Origin'] = '*'
    return response



def blog_post(request, blog_id, slug=None):
    blog = get_object_or_404(Blog, id=blog_id)
    if slug != blog.slug:
        if not blog.published:
            # For unpublished posts, you need to get the slug right for security
            raise Http404("Incorrect slug")

        # Redirect to proper slug
        return redirect(blog.get_absolute_url())

    url = request.build_absolute_uri(blog.get_absolute_url())

    # READ MORE http://open.weibo.com/widget/publisher.php
    weibo_share = "http://service.weibo.com/share/share.php?"
    weibo_share += urllib.parse.urlencode({
        'url': url,
        'title': '分享一篇弥财的文章: {}'.format(blog.title),
        'pic': '',
        'ralateUid': '3978566675'
    })
    wechat_qr = "https://api.qrserver.com/v1/create-qr-code/?"
    wechat_qr += urllib.parse.urlencode({
        'data': url,
        'size': '150x150',
    })
    mail_copy = "mailto:?"
    mail_copy += urllib.parse.urlencode({
        'subject': '给你分享一篇弥财的文章: {}'.format(blog.title),
        'body': '看看这个地址：{}'.format(url),
    })

    context = dict(
        posts=Blog.objects.filter(published=True),
        page=blog,
        title=blog.title,
        description=blog.description,
        is_blog=True,
        weibo_share=weibo_share,
        wechat_qr=wechat_qr,
        mail_copy=mail_copy
    )
    return render_to_response('blog_post.html', context)


@ensure_csrf_cookie
def risk_test(request, risktest_uuid=None):
    context = dict(
        taking_test=True
    )

    if risktest_uuid:
        try:
            risktest=RiskTest.objects.get(id=risktest_uuid)
        except RiskTest.DoesNotExist:
            return render_to_response('risktest.html')
        context['risktest'] = risktest
        context['taking_test'] = False
        portfolio = get_portfolios()[risktest.risk_level - 1]
        context['portfolio'] = portfolio

    if request.method == 'POST':
        results = request.POST
        response = {}

        if "risk_level" in results:
            # if risk_level is set then ignore the questionnaire answers
            risk_level = results["risk_level"]
            portfolio = get_portfolios()[int(risk_level) - 1]
            response['portfolio'] = portfolio
            response['risk_level'] = risk_level
        elif results:
            kwargs = dict(
                age=results["age"],
                child=results["child"],
                yearly_income=results["yearly_income"],
                liquid_assets=results["liquid_assets"],
                risk_aversion=results["risk_aversion"],
            )
            log.debug("risk test results: {}".format(results))

            risk_level = calculate_risk(**kwargs)

            portfolio = get_portfolios()[risk_level - 1]
            response['portfolio'] = portfolio
            response['risk_level'] = risk_level

        return JsonResponse(response)

    return render_to_response('risktest.html', context=context)

def risk_test_email(request):
    if request.method == 'GET':
        user = request.user
        if user and user.is_superuser:
            risk_level = request.GET.get('risk_level', '')
            context = dict(
                risk_level=risk_level,
                current_year=datetime.now().year
            )
            response_format = request.GET.get('format', 'html')
            if response_format == 'txt':
                return render_to_response('questionnaire_email.txt', context=context)
            else:
                return render_to_response('questionnaire_email.html', context=context)
        else:
            return HttpResponse('Unauthorized', status=401)

    if request.method not in ('GET', 'POST'):
        return HttpResponse('The requested method is not supported.')

    results = request.POST

    if "email" not in results:
        return HttpResponse('No email address provided')

    if "risk_level" in results:
        # if risk_level is set then ignore the questionnaire answers
        risk_level = results["risk_level"]
    else:
        required_keys = ("age", "child", "yearly_income", "liquid_assets",
                         "risk_aversion")
        if not all(k in results for k in required_keys):
            return HttpResponse("Not all required attributes was provided")

        kwargs = dict(
            age=results["age"],
            child=results["child"],
            yearly_income=results["yearly_income"],
            liquid_assets=results["liquid_assets"],
            risk_aversion=results["risk_aversion"],
        )
        log.debug("risk test results: {}".format(results))

        risk_level = calculate_risk(**kwargs)

    log.debug("sending of email initiated")

    from_email_address = 'support@micai.asia'
    to_email_address = results['email']
    subject = 'Thank for you taking our risk questionnaire'

    try:
        RiskTest.objects.get(email=to_email_address)
    except RiskTest.DoesNotExist:
        created_risk_test = RiskTest.objects.create(
            age=results["age"],
            child=results["child"],
            yearly_income=results["yearly_income"],
            liquid_assets=results["liquid_assets"],
            risk_aversion=results["risk_aversion"],
            risk_level=risk_level,
            email=to_email_address,
        )

        log.info("new risk test record has been created")

        # TODO: included risk test images (based solely on risk level)
        context = dict(
            risk_level=risk_level
        )

        # only send email once for to prevent spamming
        email_txt = render_to_string('questionnaire_email.txt', context=context)
        email_html = render_to_string('questionnaire_email.html', context=context)

        try:
            send_mail(
                subject, email_txt, from_email_address,
                [to_email_address], fail_silently=False,
                html_message=email_html)
            log.info("email has been sent")
        except BadHeaderError:
            return HttpResponse('Invalid header found.')

        return redirect(created_risk_test)

# TODO: move to something like utils
FAVICON_PATH = getattr(settings, 'FAVICON_PATH')
_icon_data = None
_icon_data_etag = None
DAY = 24 * 3600

@cache_control(max_age=DAY)
def favicon_ico(request):
    """Serves a favicon.ico from FAVICON_PATH

    Some services, like Youtube, expect a favicon to always be available
    under /favicon.ico

    Url config::

        url(r'^favicon\.ico$', 'project.website.views.favicon_ico'),

    """
    global _icon_data, _icon_data_etag
    if not FAVICON_PATH:
        return HttpResponseNotFound("No FAVICON_PATH configured")

    if _icon_data is None:
        # Load and cache icon in memory (should be small)
        icon_data = open(FAVICON_PATH, 'rb').read()
        _icon_data_etag = hashlib.md5(icon_data).hexdigest()
        _icon_data = icon_data

    response = HttpResponse(_icon_data, mimetype='image/x-icon')
    response['ETag'] = '"' + _icon_data_etag + '"'
    return response


class CustomAtomFeed(Atom1Feed):
    def add_item_elements(self, handler, item):
        super(CustomAtomFeed, self).add_item_elements(handler, item)
        # Add a new custom element named 'content' to each of the tag 'item'.
        handler.addQuickElement("content", item['content'])


class BlogFeed(Feed):
    feed_type = CustomAtomFeed
    title = "弥财"
    link = "/feed/"
    subtitle = "博客"

    def items(self):
        return Blog.objects.all()[:5]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.description

    def item_author_name(self, item):
        return item.author.name

    def item_extra_kwargs(self, item):
        """
        Returns an extra keyword arguments dictionary that is used with
        the `add_item` call of the feed generator.
        Add the 'content' field of the 'Entry' item, to be used by the
        custom feed generator.
        """
        content = "".join([c.render() for c in item.content.content])
        return {'content': content}


def dumpdata(request):
    """Dumps all site data serialized as JSON

    The output is the equivalent of:
    ./manage.py dumpdata -a --natural --format=json --indent=2 <APPS>

    The list of apps is retrieved from settings.DUMPDATA_APPS

    THIS VIEW REQUIRES SUPERUSER PRIVILEGES!
    """
    user = request.user
    if not user or not user.is_superuser:
        log.warn('dumpdata: User %s %s is not a superuser, rejecting.' % (
            user.id, user.username))
        raise PermissionDenied('You are not allowed to call this view')

    log.info(
        'dumpdata: starting dump for User %s %s' % (user.id, user.username))
    t0 = time.time()

    tf = tempfile.NamedTemporaryFile(mode="w+t")
    command = Command()
    command.handle(
        *settings.DUMPDATA_APPS,
        format='json',
        indent=2,
        exclude=[],
        use_natural_keys=True,
        use_base_manager=True,
        traceback=True,
        output=tf.name
    )

    data = tf.read()
    tf.close()

    log.info("DUMPDATA_APPS: {}".format(*settings.DUMPDATA_APPS))
    log.info("DUMPDATA: {}".format(data))

    t1 = time.time()
    ms = (t1 - t0) * 1000
    log.info(
        'dumpdata: finished dump for User %s %s (%i ms elapsed, %.1f kB)' % (
            user.id, user.username, ms, len(data) / 1024.0))

    project_name = os.path.split(os.getcwd())[1]
    ts = datetime.now().strftime('%Y%m%d-%H%M%S')
    filename = "dump-%s-%s.json" % (project_name, ts,)

    response = HttpResponse(data, content_type='application/json')
    response['Content-Disposition'] = 'attachment; filename=' + filename
    return response
