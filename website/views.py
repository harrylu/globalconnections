# coding: utf-8


from django.template import RequestContext
from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect
from django.views.generic.base import TemplateView
from django.contrib import messages

from .models import FeedBack, AboutPage, HomeImage
from .forms import FeedBackForm, ContactForm, UserProfileForm

def feedback(request):

    result = 0
    if request.method == "GET":
        form = FeedBackForm()
    else:
        form = FeedBackForm(request.POST)
        if form.is_valid():
            form.save()
            result = 1
    return render_to_response("feedback.html", {"action": "feedback", \
            "form": form, "result": result}, context_instance = RequestContext(request))


def contactus(request):

    result = 0
    if request.method == "GET":
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            result = 1
    return render_to_response("contactus.html", {"action": "contactus", \
            "form": form, "result": result}, context_instance = RequestContext(request))

def register(request):

    result = 0
    if request.method == "GET":
        form = UserProfileForm()
    else:
        form = UserProfileForm(request.POST)
        if form.is_valid():
            form.save()
            result = 1
    return render_to_response("register.html", {"action": "register", \
            "form": form, "result": result}, context_instance = RequestContext(request))

class AboutPageView(TemplateView):

    page_type = "A"
    action = "aboutus"

    def get_context_data(self, **kwargs):
        context = super(AboutPageView, self).get_context_data(**kwargs)
        context['objects'] = AboutPage.objects.filter(active = True, page_type = self.page_type).order_by('order')
        context["action"] = self.action
        if self.page_type == "T":
            context["objects"] = [context["objects"][x: x+4] for x in xrange(0, len(context['objects']) + 1, 4)]
        return context

class HomePageView(TemplateView):

    action = "index"
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        if self.request.LANGUAGE_CODE == "en":
            context['objects'] = HomeImage.objects.filter(active = True, language = 'E').order_by('order')
        else:
            context['objects'] = HomeImage.objects.filter(active = True, language = 'Z').order_by('order')
        img_dict = {}
        for x in context["objects"]:
            img_dict[x.order] = x.img.url if x.img else ''
        context["action"] = self.action
        context["objects"] = img_dict
        return context

    
