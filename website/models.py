# coding: utf-8

#import copy
#import random
#import uuid
from django.db import models
from django.contrib.sites.models import Site
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string

#from ckeditor.fields import RichTextField

from tinymce.models import HTMLField as RichTextField


class FeedBack(models.Model):
    
    content = models.CharField(_("content"), max_length = 1024)
    pinyin_name = models.CharField(_("Pin Yin"), max_length = 32)
    en_name = models.CharField(_("English Name"), max_length = 64)
    email = models.EmailField(_("Email"))
    created = models.DateTimeField(_('created on'), auto_now_add=True)
    updated = models.DateTimeField(_('last update'), auto_now=True)

class Contact(models.Model):
    
    content = models.CharField(_("Any questions"), max_length = 1024)
    pinyin_name = models.CharField(_("Pin Yin"), max_length = 32)
    en_name = models.CharField(_("English Name"), max_length = 64)
    email = models.EmailField(_("Email"))
    created = models.DateTimeField(_('created on'), auto_now_add=True)
    updated = models.DateTimeField(_('last update'), auto_now=True)

class UserProfile(models.Model):
    
    cn_name = models.CharField(_("Chinese Name"), max_length = 32)
    pinyin_name = models.CharField(_("Pin Yin"), max_length = 32)
    en_name = models.CharField(_("English Name"), max_length = 64)
    age = models.CharField(_("age"), max_length = 64)
    city = models.CharField(_("City"), max_length = 1024)
    university = models.CharField(_("university"), max_length = 1024)
    us_city = models.CharField(_("us_city"), max_length = 1024)
    went_date = models.CharField(_('went to American'), max_length = 64, blank = True, null = True)
    wechat = models.CharField(_("wechat"), max_length = 128)
    phone = models.CharField(_("phone"), max_length = 64)
    email = models.EmailField(_("Email"))
    others = models.CharField(_("others"), max_length = 1024, null = True, blank = True)
    created = models.DateTimeField(_('created on'), auto_now_add=True)
    updated = models.DateTimeField(_('last update'), auto_now=True)

from django.core.mail import send_mail
def send_email(**kwargs):
    if not kwargs.get("created"):
        return
    instance = kwargs.get("instance")
    class_name =  kwargs.get("sender").__name__
    class_name = "user rigster" if class_name == "UserProfile" else class_name.lower()
    title = "There is a {0} for you to check".format(class_name)
    message = u"中文姓名:{0}, 英文姓名:{1}，邮箱:{2}, 内容:{3}, URL:{4}".format(
            getattr(instance,'cn_name', '') or getattr(instance, "pinyin_name", ""), instance.en_name, instance.email, 
            getattr(instance, "content", ""),
            "http://{0}/admin/website/{1}/{2}/".format(Site.objects.get_current().domain, 
                instance.__class__.__name__.lower(), instance.id)
            )
    context = {
            "object": [[x.name, x.value_from_object(instance)] for x in instance._meta.fields],
            "url": "http://{0}/admin/website/{1}/{2}/".format(Site.objects.get_current().domain, 
                instance.__class__.__name__.lower(), instance.id)
            }
    subject = render_to_string('email_body.html', context)
    send_mail(title,'', 'admin@globalconnections.com',
            [x["email"] for x in User.objects.filter(is_staff = True).values("email")], 
            fail_silently=False, 
            html_message = subject)


post_save.connect(send_email, sender = FeedBack)
post_save.connect(send_email, sender = Contact )
post_save.connect(send_email, sender = UserProfile)



ABOUT_PAGE_TYPE = (
    ('A', 'About'),
    ('Q', 'Q&A'),
    ('T', 'Testimonials'),
    ('C', 'American culture'),
    ('S', 'Skills'),
    )
class AboutPage(models.Model):

    page_type = models.CharField(_("Category"), max_length = 1, default='A', choices = ABOUT_PAGE_TYPE)
    active = models.BooleanField(_("active"), help_text=_(""),
        default=True)

    order = models.IntegerField(default = 1)
    title = RichTextField(_("title"), 
        blank=True,
        help_text=_("H2 for title, P for content"))

    cn_title = RichTextField(_("Chinese Title"), 
        blank=True,
        help_text=_("H2 for title, P for content"))

    body = RichTextField(_("Body"), 
        blank=True,
        help_text=_("H2 for title, P for content"))

    cn_body = RichTextField(_("Chinese Body"), 
        blank=True,
        help_text=_("H2 for title, P for content"))

    img = models.ImageField(_("image"),
        blank=True, null=True,
        upload_to='uploads',
        help_text=_("Image"))

    name = RichTextField(_("Name"), 
        blank=True,
        help_text=_("H2 for title, P for content"))

    cn_name = RichTextField(_("Chinese Name"), 
        blank=True,
        help_text=_("H2 for title, P for content"))

    def __str__(self):

        return self.page_type

    class Meta:

        verbose_name = "Page"
        verbose_name_plural  = "Pages"

class HTMLMeta(models.Model):

    content = models.CharField(_("content"), max_length = 1024)
    name = models.CharField(_("name"), max_length = 64)
    active = models.BooleanField(_("active"), help_text=_(""),
        default=True)

    class Meta:

        verbose_name = "Meta"
        verbose_name_plural  = "Meta"

LANGUAGE_CODE = (
    ('E', 'EN'),
    ('Z', 'ZH-CN'),
    )

class HomeImage(models.Model):

    language = models.CharField(_("language"), max_length = 1, default='E', choices = LANGUAGE_CODE)
    active = models.BooleanField(_("active"), help_text=_(""),
        default=True)

    order = models.IntegerField(default = 1)

    title = RichTextField(_("title"), 
        blank=True,
        help_text=_("H2 for title, P for content"))

    img = models.ImageField(_("image"),
        blank=True, null=True,
        upload_to='uploads',
        help_text=_("Image"))
