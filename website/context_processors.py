# coding: utf-8

from .models import HTMLMeta

def CommonMeta(request):
    
    context = {'metas': HTMLMeta.objects.filter(active = True)}
    return context

