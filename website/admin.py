# -*- coding:utf-8 -*-


from django.contrib import admin

from .models import FeedBack, Contact, UserProfile, AboutPage, HTMLMeta, HomeImage

@admin.register(FeedBack)
class FeedBackAdmin(admin.ModelAdmin):

    list_display = ["pinyin_name", "en_name", "content", "email", "created"]

@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):

    list_display = ["pinyin_name", "en_name", "content", "email", "created"]

@admin.register(HTMLMeta)
class HTMLMetaAdmin(admin.ModelAdmin):

    list_display = ["name", "content", "active"]

@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):

    list_display = ["cn_name", "pinyin_name", "en_name", "city", "university", "us_city", "wechat", "phone", "email", "others", "created"]

@admin.register(AboutPage)
class AboutPageAdmin(admin.ModelAdmin):

    list_display = ['page_type', '_safe_title', 'active']
    def _safe_title(self, obj):
         return obj.title
    _safe_title.allow_tags = True

@admin.register(HomeImage)
class HomeImageAdmin(admin.ModelAdmin):

    list_display = ['language', 'active', 'order']
