# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0004_auto_20150819_1438'),
    ]

    operations = [
        migrations.CreateModel(
            name='AboutPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('page_type', models.CharField(default=b'A', max_length=1, verbose_name='Category', choices=[(b'A', b'About'), (b'Q', b'Q&A'), (b'T', b'Testimonials')])),
                ('active', models.BooleanField(default=False, help_text='', verbose_name='active')),
                ('title', ckeditor.fields.RichTextField(help_text='H2 for title, P for content', verbose_name='title', blank=True)),
                ('body', ckeditor.fields.RichTextField(help_text='H2 for title, P for content', verbose_name='Body', blank=True)),
                ('img', models.ImageField(help_text='Image', upload_to=b'itinerary', null=True, verbose_name='image', blank=True)),
                ('name', ckeditor.fields.RichTextField(help_text='H2 for title, P for content', verbose_name='Name', blank=True)),
            ],
            options={
                'verbose_name': 'Page',
                'verbose_name_plural': 'Pages',
            },
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='others',
            field=models.CharField(max_length=1024, null=True, verbose_name='others', blank=True),
        ),
    ]
