# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0005_auto_20150820_0600'),
    ]

    operations = [
        migrations.AddField(
            model_name='aboutpage',
            name='order',
            field=models.IntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='active',
            field=models.BooleanField(default=False, help_text='', verbose_name='active'),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='body',
            field=tinymce.models.HTMLField(help_text='H2 for title, P for content', verbose_name='Body', blank=True),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='img',
            field=models.ImageField(help_text='Image', upload_to=b'uploads', null=True, verbose_name='image', blank=True),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='name',
            field=tinymce.models.HTMLField(help_text='H2 for title, P for content', verbose_name='Name', blank=True),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='title',
            field=tinymce.models.HTMLField(help_text='H2 for title, P for content', verbose_name='title', blank=True),
        ),
    ]
