# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0002_contact'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cn_name', models.CharField(max_length=32, verbose_name='Chinese Name')),
                ('pinyin_name', models.CharField(max_length=32, verbose_name='Pin Yin')),
                ('en_name', models.CharField(max_length=64, verbose_name='English Name')),
                ('city', models.CharField(max_length=1024, verbose_name='City')),
                ('university', models.CharField(max_length=1024, verbose_name='university')),
                ('us_city', models.CharField(max_length=1024, verbose_name='us_city')),
                ('wechat', models.CharField(max_length=128, verbose_name='wechat')),
                ('phone', models.CharField(max_length=64, verbose_name='phone')),
                ('email', models.EmailField(max_length=254, verbose_name='Email')),
                ('others', models.CharField(max_length=64, verbose_name='others')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created on')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='last update')),
            ],
        ),
    ]
