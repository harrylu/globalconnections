# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0008_auto_20150824_0703'),
    ]

    operations = [
        migrations.AddField(
            model_name='aboutpage',
            name='cn_body',
            field=tinymce.models.HTMLField(help_text='H2 for title, P for content', verbose_name='Chinese Body', blank=True),
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='cn_name',
            field=tinymce.models.HTMLField(help_text='H2 for title, P for content', verbose_name='Chinese Name', blank=True),
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='cn_title',
            field=tinymce.models.HTMLField(help_text='H2 for title, P for content', verbose_name='Chinese Title', blank=True),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='active',
            field=models.BooleanField(default=True, help_text='', verbose_name='active'),
        ),
        migrations.AlterField(
            model_name='htmlmeta',
            name='active',
            field=models.BooleanField(default=True, help_text='', verbose_name='active'),
        ),
    ]
