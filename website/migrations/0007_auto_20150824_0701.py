# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0006_auto_20150820_0643'),
    ]

    operations = [
        migrations.CreateModel(
            name='HTMLMeta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.CharField(max_length=1024, verbose_name='content')),
                ('name', models.CharField(max_length=64, verbose_name='name')),
            ],
            options={
                'verbose_name': 'Meta',
                'verbose_name_plural': 'Meta',
            },
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='active',
            field=models.BooleanField(default=False, help_text='', verbose_name='active'),
        ),
    ]
