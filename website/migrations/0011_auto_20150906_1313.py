# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0010_auto_20150901_1434'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='age',
            field=models.CharField(default='', max_length=64, verbose_name='age'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='went_date',
            field=models.DateTimeField(null=True, verbose_name='went to American', blank=True),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='active',
            field=models.BooleanField(default=True, help_text='', verbose_name='active'),
        ),
        migrations.AlterField(
            model_name='homeimage',
            name='active',
            field=models.BooleanField(default=True, help_text='', verbose_name='active'),
        ),
        migrations.AlterField(
            model_name='htmlmeta',
            name='active',
            field=models.BooleanField(default=True, help_text='', verbose_name='active'),
        ),
    ]
