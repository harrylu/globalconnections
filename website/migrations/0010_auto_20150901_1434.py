# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0009_auto_20150824_0725'),
    ]

    operations = [
        migrations.CreateModel(
            name='HomeImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language', models.CharField(default=b'E', max_length=1, verbose_name='language', choices=[(b'E', b'EN'), (b'Z', b'ZH-CN')])),
                ('active', models.BooleanField(default=True, help_text='', verbose_name='active')),
                ('order', models.IntegerField(default=1)),
                ('title', tinymce.models.HTMLField(help_text='H2 for title, P for content', verbose_name='title', blank=True)),
                ('img', models.ImageField(help_text='Image', upload_to=b'uploads', null=True, verbose_name='image', blank=True)),
            ],
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='active',
            field=models.BooleanField(default=True, help_text='', verbose_name='active'),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='page_type',
            field=models.CharField(default=b'A', max_length=1, verbose_name='Category', choices=[(b'A', b'About'), (b'Q', b'Q&A'), (b'T', b'Testimonials'), (b'C', b'American culture'), (b'S', b'Skills')]),
        ),
        migrations.AlterField(
            model_name='htmlmeta',
            name='active',
            field=models.BooleanField(default=True, help_text='', verbose_name='active'),
        ),
    ]
