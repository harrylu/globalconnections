# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0011_auto_20150906_1313'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aboutpage',
            name='active',
            field=models.BooleanField(default=True, help_text='', verbose_name='active'),
        ),
        migrations.AlterField(
            model_name='homeimage',
            name='active',
            field=models.BooleanField(default=True, help_text='', verbose_name='active'),
        ),
        migrations.AlterField(
            model_name='htmlmeta',
            name='active',
            field=models.BooleanField(default=True, help_text='', verbose_name='active'),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='went_date',
            field=models.CharField(max_length=64, null=True, verbose_name='went to American', blank=True),
        ),
    ]
