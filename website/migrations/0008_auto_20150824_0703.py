# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0007_auto_20150824_0701'),
    ]

    operations = [
        migrations.AddField(
            model_name='htmlmeta',
            name='active',
            field=models.BooleanField(default=True, help_text='', verbose_name='active'),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='active',
            field=models.BooleanField(default=True, help_text='', verbose_name='active'),
        ),
    ]
