# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FeedBack',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.CharField(max_length=1024, verbose_name='content')),
                ('pinyin_name', models.CharField(max_length=32, verbose_name='Pin Yin')),
                ('en_name', models.CharField(max_length=64, verbose_name='English Name')),
                ('email', models.EmailField(max_length=254, verbose_name='Email')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created on')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='last update')),
            ],
        ),
    ]
