# -*- coding: utf-8 -*-


import re

from django.forms import ModelForm, Form
from django import forms

from .models import FeedBack, Contact, UserProfile

class FeedBackForm(ModelForm):
   
    content = forms.CharField(label = u'content', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}))
    pinyin_name = forms.CharField(label = u'Pinyin Name', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}))
    en_name = forms.CharField(label = u'English Name', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}))
    email = forms.EmailField(label = u'Your Email', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}))

    class Meta:
        model = FeedBack
        fields = ('content', 'pinyin_name', 'en_name', 'email', )

class ContactForm(ModelForm):
   
    content = forms.CharField(label = u'content', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}))
    pinyin_name = forms.CharField(label = u'Pinyin Name', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}))
    en_name = forms.CharField(label = u'English Name', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}))
    email = forms.EmailField(label = u'Your Email', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}))

    class Meta:
        model = Contact
        fields = ('content', 'pinyin_name', 'en_name', 'email', )
    
class UserProfileForm(ModelForm):
   
    cn_name = forms.CharField(label = u'Chinese Name', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}))
    pinyin_name = forms.CharField(label = u'Pinyin Name', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}))
    en_name = forms.CharField(label = u'English Name', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}))
    age = forms.CharField(label = u'Age', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}), required = False)
    city = forms.CharField(label = u'content', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}), required = False)
    university = forms.CharField(label = u'content', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}))
    us_city = forms.CharField(label = u'content', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}))
    wechat = forms.CharField(label = u'content', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}), required = False)
    others = forms.CharField(label = u'content', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}), required = False)
    phone = forms.CharField(label = u'phone', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}), required = False)
    email = forms.EmailField(label = u'Your Email', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}))
    went_date = forms.CharField(label = u'You will go to went to America', widget = forms.TextInput(attrs = {"class" : "search", "url" : "123"}), required = False)

    class Meta:
        exclude = ['created', 'updated']
        model = UserProfile
